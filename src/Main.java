class NegativeNumberException extends Exception {}
class TooLongNumberException extends Exception {}

class Main {

    /** Funkcja zwraca silnie podanej liczby.
     *
     * @param digit liczba dla ktorej obliczania jest silnia
     * @return silnia
     * @throws NegativeNumberException wyjatek zwracany gdy parametr jest ujemny
     * @throws TooLongNumberException wyjatek zwracany gdy dla danego parametru silnia nie bedzie obliczona
     */
    private long getFactorial(final long digit) throws NegativeNumberException, TooLongNumberException {

        if(digit < 0) {
            throw new NegativeNumberException();
        } else if (digit > 20) {
            throw new TooLongNumberException();
        } else {
            if(digit == 0) {
                return 1;
            } else {
                return ( digit * getFactorial(digit-1) );
            }
        }
    }

    public static void main(String[] args) {

        long digit = 4;
        Main main = new Main();

        try {

            long factorial = main.getFactorial(digit);
            System.out.printf("Silnia(%d) = %d \n", digit, factorial);

        } catch(NegativeNumberException e) {
            System.out.println("Wprowadzona liczba jest ujemna.");
        } catch(TooLongNumberException e) {
            System.out.println("Dla tej liczby silnia nie zostanie policzona.");
        }
    }
}